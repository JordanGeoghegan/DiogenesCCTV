#!/bin/sh
# $Diogenes: timelapse.sh 0.1 2020/04/21 16:55:00 sparx $

# Copyright (c) 2020 Jordan Geoghegan <jordan@geoghegan.ca>

# Permission to use, copy, modify, and/or distribute this software for any 
# purpose with or without fee is hereby granted, provided that the above 
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM 
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE 
# OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
# PERFORMANCE OF THIS SOFTWARE.

web_dir="/var/www/dio"
rundate=$(date "+%Y-%m-%d_%H:%M:%S")

for friendlyname in "$web_dir"/video-recordings/* ; do

	namedpipe="/tmp/$RANDOM$RANDOM-Diogenes-FIFO"
	mkfifo "$namedpipe" || exit 1
	mkdir -p "$web_dir"/timelapse/"${friendlyname##*/}"
	nice -n 20 ffmpeg -hwaccel auto -f mpegts -i "$namedpipe" -filter:v "setpts=PTS/50" -c:v libx264 -preset ultrafast -an -sn "$web_dir"/timelapse/"${friendlyname##*/}"/"$rundate".mp4 &

	for filename in "$web_dir"/video-recordings/"${friendlyname##*/}"/*.mp4 ; do
		nice -n 20 ffmpeg -y -i "$filename" -c:v copy -an -sn -bsf:v h264_mp4toannexb -f mpegts "$namedpipe" 2>/dev/null
	done

	sleep 5
	pkill -f "ffmpeg -hwaccel auto -f mpegts -i $namedpipe"
	sleep 10
	pkill -9 -f "ffmpeg -hwaccel auto -f mpegts -i $namedpipe"
	rm -f "$namedpipe"
done
