// $Diogenes: diogenes.go 0.1 2020/05/17 07:00:00 geoghegan $

package main

import (
        "fmt"
        "os"
        "os/exec"
	"path/filepath"
	"time"
	"sort"
	"strconv"
	"github.com/spf13/viper"
)

// DiogenesCCTV Configuration exported
type Configurations struct {
    OS_TYPE     string
    WEB_DIR     string
    DISK_QUOTA  int64
    PURGE_NUM   int64	
}

// h264 Camera Configuration exported
type H264camstruct struct {
    Friendlyname  string
    ENABLED	  string
    URL  	  string
    Buffer_depth  string
    HW_accel      string
    Video_codec	  string
    Audio_codec	  string
    HlsChunkSize  string
    SegmentSize	  string
    Preset	  string
    Resolution	  string	
}

type H264Config struct {
    H264cams []H264camstruct
}

// V4l2 Camera Configuration exported
type V4l2_cam struct {
    Friendlyname  string
    ENABLED	  string
    Path  	  string
    Buffer_depth  string
    HW_accel      string
    Video_codec	  string
    Audio_codec	  string
    HlsChunkSize  string
    SegmentSize	  string
    Preset	  string
    Resolution	  string	
}

type V4l2Config struct {
    V4l2_cams []V4l2_cam
}

// Single Frame JPEG Camera Configuration exported
type Jpeg_net_cam struct {
    Friendlyname  string
    ENABLED	  string
    URL 	  string
    Buffer_depth  string
    HW_accel      string
    HlsChunkSize  string
    SegmentSize	  string
    Preset	  string
    Resolution	  string	
}

type JpegConfig struct {
    Jpeg_net_cams []Jpeg_net_cam
}

// Functions required for StoragePurge goroutine (sorts files by creation time)
type ByModTime []os.FileInfo

func (fis ByModTime) Len() int {
    return len(fis)
}

func (fis ByModTime) Swap(i, j int) {
    fis[i], fis[j] = fis[j], fis[i]
}

func (fis ByModTime) Less(i, j int) bool {
    return fis[i].ModTime().Before(fis[j].ModTime())
}

func main() {

	// Set the file name of the configurations file
	viper.SetConfigName("diogenes.conf")

	// Set the path to look for the configurations file
	viper.AddConfigPath("/etc/")

	// Enable VIPER to read Environment Variables
	viper.AutomaticEnv()

	// Set clean variable names and declare config file format
	viper.SetConfigType("yml")
	var configuration Configurations
	var h264config H264Config
	var v4l2config V4l2Config
	var jpegconfig JpegConfig

	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file, %s", err)
	}
	
	// Set undefined variables
	viper.SetDefault("configuration.PURGE_NUM", "5")


	err := viper.Unmarshal(&configuration)
	if err != nil {
		fmt.Printf("Unable to decode into struct, %v", err)
	}

	err = viper.Unmarshal(&h264config)
	if err != nil {
		fmt.Printf("Unable to decode into struct, %v", err)
	}

	err = viper.Unmarshal(&v4l2config)
	if err != nil {
		fmt.Printf("Unable to decode into struct, %v", err)
	}

	err = viper.Unmarshal(&jpegconfig)
	if err != nil {
		fmt.Printf("Unable to decode into struct, %v", err)
	}


	// Format to a string by passing the number and it's base.
	diskquota := strconv.FormatInt(configuration.DISK_QUOTA, 10)

	// DEBUGGING: Print out parsed h264 camera keys
	for _, h := range h264config.H264cams {
		fmt.Printf("WWW: %s, OS: %s, NAME: %s, STATUS: %s, URL: %s, VIDEO_CODEC %s, AUDIO_CODEC %s, HlsChunkSize: %s, SegmentSize: %s, FFmpeg Preset: %s, Resolution: %s\n\n\n", configuration.WEB_DIR, configuration.OS_TYPE, h.Friendlyname, h.ENABLED, h.URL, h.Video_codec, h.Audio_codec, h.HlsChunkSize, h.SegmentSize, h.Preset, h.Resolution)
	}
	fmt.Println(configuration.WEB_DIR, configuration.OS_TYPE)

	// Ensure video-recordings directory exists
	if _, err := os.Stat(configuration.WEB_DIR + "/video-recordings/"); os.IsNotExist(err) {
		os.Mkdir(configuration.WEB_DIR + "/video-recordings/", 0755)
	}

	// Spin subsystems off into their own goroutines

	// Storage Quota enforcement goroutine
        go func() {
          for {
                time.Sleep(180 * time.Second)
                var size int64
                err = filepath.Walk(configuration.WEB_DIR + "/video-recordings/", func(_ string, info os.FileInfo, err error) error {
                        if err != nil {
                                return err
                        }
                        if !info.IsDir() {
                                size += info.Size()
                        }
                        return err
                })
		sizeMB := int64(size) / 1048576
		if sizeMB > configuration.DISK_QUOTA {
		fmt.Println("Running StoragePurge function")
		  for i := range h264config.H264cams {
		    h264 := h264config.H264cams[i]
                    if h264.ENABLED == "TRUE" {
                  	f, _ := os.Open(configuration.WEB_DIR + "/video-recordings/" + h264.Friendlyname + "/")
                  	fis, _ := f.Readdir(-1)
                  	f.Close()
                  	sort.Sort(ByModTime(fis))
			for _, fi := range fis[:configuration.PURGE_NUM] {
				os.Remove(configuration.WEB_DIR + "/video-recordings/" + h264.Friendlyname + "/" + fi.Name())
			fmt.Println("PURGED:", configuration.WEB_DIR + "/video-recordings/" + h264.Friendlyname + "/" + fi.Name())
                    }
                  }
                }
            }
          }
        }()


	// HTML Render routine
	go func() {
	    for {
		time.Sleep(60 * time.Second)
		fmt.Println("Running HTML Renderer!")
		// We pass user defined info from /etc/diogenes.conf to www-render.sh as arguments
		cmd := exec.Command("nice", "-n20", "/bin/sh", "/home/_diogenes/sh/www-render.sh", configuration.OS_TYPE, configuration.WEB_DIR, diskquota)
        	cmd.Stdout = os.Stdout
        	cmd.Stderr = os.Stderr
        	err := cmd.Run()
        	if err != nil {
		// Handle Error Here...
		}
	    }
	}()

	// H264 Master-Shim Enumeration
	for i := range h264config.H264cams {
		h264 := h264config.H264cams[i]
		if h264.ENABLED == "TRUE" {
		  go func() {
		    for {
			time.Sleep(1 * time.Second)
			fmt.Printf("Starting %s Shim!\n", h264.Friendlyname)
			// Run shim
			cmd := exec.Command("/bin/sh", "/home/_diogenes/sh/h264-master-shim.sh", h264.Friendlyname, h264.URL, h264.Video_codec, h264.Audio_codec, h264.Resolution, h264.Preset, h264.HW_accel, h264.Buffer_depth)
        		cmd.Stdout = os.Stdout
        		cmd.Stderr = os.Stderr
        		err := cmd.Run()
        		if err != nil {
			// Handle Error Here...
			}
		    }
		  }()
		}
        } 

	// H264 Camera Enumeration
	for i := range h264config.H264cams {
		h264 := h264config.H264cams[i]
		if h264.ENABLED == "TRUE" {
		  go func() {
		    for {
			time.Sleep(5 * time.Second)
			fmt.Printf("Starting %s Camera!\n", h264.Friendlyname)

			// Clear contents of camera HLS directory
			if _, err := os.Stat(configuration.WEB_DIR + "/hls/" + h264.Friendlyname); !os.IsNotExist(err) {
			
			  // Set target directory.
			  directory := fmt.Sprintf(configuration.WEB_DIR + "/hls/" + h264.Friendlyname + "/")

			  // Open the directory and read all its files.
			  dirRead, _ := os.Open(directory)
			  dirFiles, _ := dirRead.Readdir(0)

			  // Loop over the directory's files.
			  for index := range(dirFiles) {
			  fileHere := dirFiles[index]

			  // Get name of file and its full path.
			  fileName := fileHere.Name()
			  fullPath := directory + fileName

			  // Remove the file.
			  os.Remove(fullPath)
			  fmt.Println("Removed file:", fullPath)
			  }
		  	}

			// Ensure HLS directory exists
			if _, err := os.Stat(configuration.WEB_DIR + "/hls/" + h264.Friendlyname); os.IsNotExist(err) {
    				os.Mkdir(configuration.WEB_DIR + "/hls/" + h264.Friendlyname, 0755)
			}

			// Ensure Video Recording directory exists
			if _, err := os.Stat(configuration.WEB_DIR + "/video-recordings/" + h264.Friendlyname); os.IsNotExist(err) {
    				os.Mkdir(configuration.WEB_DIR + "/video-recordings/" + h264.Friendlyname, 0755)
			}

			// Run FFmpeg
			cmd := exec.Command("nice", "-n9", "ffmpeg", "-y", "-hwaccel", h264.HW_accel, "-hide_banner", "-loglevel", "warning", "-err_detect", "aggressive", "-fflags", "discardcorrupt", "-i", "unix:/home/_diogenes/util/IPC/" + h264.Friendlyname + "/" + h264.Friendlyname + "-main.sock", "-c", "copy", "-f", "hls", "-hls_time", h264.HlsChunkSize, "-hls_list_size", "5", "-hls_flags", "delete_segments+split_by_time", configuration.WEB_DIR + "/hls/" + h264.Friendlyname  + "/" + h264.Friendlyname + "_" + ".m3u8", "-c", "copy", "-f", "segment", "-segment_time", h264.SegmentSize, "-segment_atclocktime", "1", "-reset_timestamps", "1", "-strftime", "1", configuration.WEB_DIR + "/video-recordings/" + h264.Friendlyname + "/" + "%Y-%m-%d_%H:%M:%S_" + h264.Friendlyname + ".mp4")
        		cmd.Stdout = os.Stdout
        		cmd.Stderr = os.Stderr
        		err := cmd.Run()
        		if err != nil {
			// Handle Error Here...
			}
		    }
		  }()
		}
        }

	// Occupy main function so goroutines can run
	for {
	time.Sleep(2000 * time.Second)
	}

}
