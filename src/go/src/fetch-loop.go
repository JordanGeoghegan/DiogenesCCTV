// $Diogenes: fetch-loop.go 0.1 2020/04/24 20:10:00 geoghegan $

package main

import (
        "fmt"
        "io"
        "net/http"
        "os"
        "time"
)

func main() {
        if len(os.Args) != 2 {
                fmt.Println("usage: fetch-loop interval(ms) URL")
                os.Exit(1)
        }

	interval := os.Args[1]
        url := os.Args[2]
        
        for {
        err := DownloadFile(url)
        time.Sleep(interval * time.Millisecond)
        if err != nil {
                panic(err)
        }
}
}

// DownloadFile will download a url and store it in local filepath.
// It writes to the destination file as it downloads it, without
// loading the entire file into memory.
func DownloadFile(url string, filepath string) error {
        // Get the data
        resp, err := http.Get(url)
        if err != nil {
                return err
        }
        defer resp.Body.Close()

        // Write the body to stdout
        _, err = io.Copy(os.Stdout, resp.Body)
        if err != nil {
                return err
        }

        return nil
}
