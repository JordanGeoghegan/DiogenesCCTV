#!/bin/sh
# $Diogenes: start-diogenes.sh 0.1 2020/04/25 00:15:00 geoghegan $

# Make sure we're running as "_diogenes" user
if [ "$(whoami)" != "_diogenes" ]; then
	printf "\n\nScript must be run as user \"_diogenes\"\nExiting...\n\n" ; exit 1
fi

cd /home/_diogenes/go && go run src/diogenes.go
